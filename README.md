# GET RICH APP

A Flutter project.
-Press the [Make it Rain] button to generate a random number of cash!
-Press the [Reset] button to reset it.
-Added animation effect by waiting for 100 microseconds before adding the next number.

![GIF](https://media.giphy.com/media/2dm65Pu7IgafdzP9l4/giphy.gif)

## Getting Started

For help getting started with Flutter, Check it online!
[documentation](https://flutter.io/).
