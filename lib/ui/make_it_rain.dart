import 'package:flutter/material.dart';
import 'dart:math';
import 'dart:async';

class MakeItRain extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return new MakeItRainState();
  }
}

class MakeItRainState extends State<MakeItRain> {
  int _moneyCounter = 0;
  var _moneyTextStyle = new TextStyle(
      fontSize: 80, fontWeight: FontWeight.w300, color: Colors.grey.shade300);

  void _updateTextStyle() {
    if (_moneyCounter > 80000) {
      _moneyTextStyle = new TextStyle(
          fontSize: 80, fontWeight: FontWeight.w900, color: Colors.green);
    } else if (_moneyCounter > 40000) {
      _moneyTextStyle = new TextStyle(
          fontSize: 80,
          fontWeight: FontWeight.w600,
          color: Colors.orange);
    } else {
      _moneyTextStyle = new TextStyle(
          fontSize: 80, 
          fontWeight: FontWeight.w400, 
          color: Colors.red);
    }
  }

  Future _rainMoney() async {
    var rng = new Random();
    int oldValue = _moneyCounter;
    int newValue = rng.nextInt(100000);
    while (_moneyCounter < oldValue + newValue) {
      await new Future.delayed(const Duration(microseconds: 100));
      setState(() {
        _moneyCounter = _moneyCounter + 1;
        _updateTextStyle();
      });
    }
  }

  void _reset() {
    setState(() {
      _moneyCounter = 0;
      _updateTextStyle();
    });
  }

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      appBar: new AppBar(
        title: Text(
          "Make It Rain",
          style: new TextStyle(color: Colors.blueGrey.shade900),
        ),
        backgroundColor: Colors.limeAccent,
      ),
      body: new Container(
        child: new Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            new Center(
              child: new Padding(
                padding: EdgeInsets.all(30),
                child: new Text(
                  "GET RICH!",
                  style: new TextStyle(
                    fontSize: 55,
                    fontWeight: FontWeight.w900,
                  ),
                ),
              ),
            ),
            new Expanded(
              child: new Center(
                  child: new Text(
                "$_moneyCounter\$",
                style: _moneyTextStyle,
              )),
            ),
            new Expanded(
              child: new Column(
                children: <Widget>[
                  new Center(
                      child: new FlatButton(
                    onPressed: () => _rainMoney(),
                    splashColor: Colors.blueGrey.shade800,
                    color: Colors.limeAccent,
                    padding: EdgeInsets.all(10),
                    child: new Text(
                      "MAKE IT RAIN",
                      style: new TextStyle(
                        fontSize: 40,
                        fontWeight: FontWeight.w900,
                      ),
                    ),
                  )),
                  new Center(
                      child: new FlatButton(
                    onPressed: () => _reset(),
                    splashColor: Colors.blueGrey.shade800,
                    color: Colors.limeAccent.shade400,
                    padding: EdgeInsets.all(10),
                    child: new Text(
                      "RESET",
                      style: new TextStyle(
                        fontSize: 40,
                        fontWeight: FontWeight.w900,
                      ),
                    ),
                  ))
                ],
              ),
            )
          ],
        ),
      ),
    );
  }
}
