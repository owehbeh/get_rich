import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';

class Login extends StatefulWidget {
  _LoginState createState() => _LoginState();
}

class _LoginState extends State<Login> {
  final TextEditingController _userController = new TextEditingController();
  final TextEditingController _passwordController = new TextEditingController();
  String _welcomeString = "Login so I can spell your name";
  
  _erase() {
    setState(() {
      _userController.clear();
      _passwordController.clear();
    });
  }

  _login() {
    setState(() {
      if(_userController.text.isEmpty || _passwordController.text.isEmpty){
        _welcomeString = "All fields are required";
      }else{
        String _tempName= _userController.text;
        _welcomeString = 'Hello $_tempName, thank you for logging in!';
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: new AppBar(
          title: Text("PLEASE LOGIN"),
          centerTitle: true,
          backgroundColor: Colors.red.shade900),
      backgroundColor: Colors.red.shade50,
      body: ListView(
        children: <Widget>[
          Container(
            alignment: Alignment.topCenter,
            child: new Column(
              children: <Widget>[
                new Padding(
                  padding: EdgeInsets.fromLTRB(0.0, 50.0, 0.0, 0.0),
                  child: new Image.asset(
                    'images/face.png',
                    height: 90.0,
                    width: 90.0,
                    color: Colors.red.shade400,
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.fromLTRB(0.0, 0.0, 0.0, 20.0),
                  child: new SpinKitPumpingHeart(color: Colors.red.shade400),
                ),
                new Container(
                  height: 180.0,
                  width: 380.0,
                  color: Colors.white,
                  child: new Column(
                    children: <Widget>[
                      new TextField(
                        controller: _userController,
                        decoration: new InputDecoration(
                            hintText: 'Username',
                            icon: new Icon(Icons.person),
                            hasFloatingPlaceholder: true),
                        cursorColor: Colors.red,
                        style: new TextStyle(color: Colors.red),
                      ),
                      new TextField(
                        controller: _passwordController,
                        decoration: new InputDecoration(
                          hintText: 'Password',
                          icon: new Icon(Icons.lock),
                        ),
                        cursorColor: Colors.red,
                      ),
                      Padding(
                        padding: const EdgeInsets.fromLTRB(0.0, 20.0, 0.0, 0.0),
                        child: new Row(
                          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                          children: <Widget>[
                            new FlatButton(
                              child: Text("Login"),
                              onPressed: _login,
                              color: Colors.red,
                              textColor: Colors.white,
                            ),
                            new FlatButton(
                              child: Text("Clear"),
                              onPressed: _erase,
                              color: Colors.red.shade300,
                              textColor: Colors.white,
                            ),
                          ],
                        ),
                      )
                    ],
                  ),
                ),
              ],
            ),
          ),
          new Row(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: <Widget>[
              Padding(
                padding: const EdgeInsets.all(20.0),
                child: new Text(
                  _welcomeString,
                  textDirection: TextDirection.ltr,
                  style: new TextStyle(color: Colors.red.shade300),
                ),
              )
            ],
          )
        ],
      ),
    );
  }
}
